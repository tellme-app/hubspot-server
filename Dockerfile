FROM node:12-alpine
WORKDIR /app
COPY package*.json ./
COPY tsconfig.json .
RUN npm install
COPY src ./src
RUN npm run build
ENV HUBSPOT_API_KEY 2715587f-bc96-4e40-a6b0-d3df1fc7e602
ENV PORTAL_ID 7902680
ENV FORM_ID fd1542df-2b57-4b8d-b8c6-98f72e3be8fd
EXPOSE 3000
CMD ["node", "dist/server.js"]
