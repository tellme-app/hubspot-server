import express = require("express");

import back = require("./back");

const routes = express.Router();

routes.post('/contact', back.createContact)

export {routes}
