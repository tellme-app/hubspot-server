import express = require("express");
import routes = require("./routes");
import cors = require("cors");
import bodyParser = require("body-parser");


const app: express.Application = express();

app.get("/", (req, res) => {
    res.send('Hello World');
})

app.use(bodyParser.json())
app.use(cors());
app.use(routes.routes);

app.listen(3000, () => {
    console.log('app listen on 3000 port')
})

