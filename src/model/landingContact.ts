export interface LandingContact {
    email: string,
    name: string,
    socialMedia: string
}
