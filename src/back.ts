import Hubspot = require("hubspot");
import {LandingContact} from "./model/landingContact";

const hubspotApiKey = process.env.HUBSPOT_API_KEY;
const formId = process.env.FORM_ID;
const portalId = process.env.PORTAL_ID;

// @ts-ignore
const hubspot = new Hubspot({
    apiKey: hubspotApiKey
})

export const createContact = (req, res) => {
    const contactToBeCreated: LandingContact = req.body;
    const hubspotData = {
        fields: [
            {
                name: "firstname",
                value: contactToBeCreated.name
            }, {
                name: "email",
                value: contactToBeCreated.email
            }, {
                name: "socialmedia",
                value: contactToBeCreated.socialMedia
            }
        ]
    }
    hubspot.forms
        .submit(portalId, formId, hubspotData)
        .catch((errResponse) => {
            res.status(errResponse.statusCode)
            res.send({
                errors: errResponse.error.errors
            });
        })
        .then((response) => res.send())
}
